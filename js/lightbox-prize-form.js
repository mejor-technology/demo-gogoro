$(document).ready(function () {
  // 判斷是否為實體獎品
  var isEntity = false;

  // 確認中獎表單是否資料都有填妥

  // 實體獎品
  $('#entity_prize_form_subnit_btn').bind('click', function (e) {
    e.preventDefault();
    // 實體獎品
    isEntity = true;

    // var lastName = $('#last_name_input').val();
    // var firstName = $('#name_input').val();
    // var cellphoneNumber = $('#cellphone_input').val();
    // var address = $('#address_input').val();
    // var city = $('#city_input').val();
    // var state = $('#state_input').val();
    // var postalCode = $('#postal_code_input').val();
    // var allPass = true;
    var allPassCount = 0;

    // 檢查==============================================

    // 全部一起檢查
    $('.form_input').each(function () {
      var errorDom = $(this).data('error');
      var value = $(this).val();

      if (isEmpty(value)) {
        $(errorDom).removeClass('hidden');
      } else {
        $(errorDom).addClass('hidden');
        allPassCount++;
      }
    });

    // // 檢查姓氏
    // if (isEmpty(lastName)) {
    //   allPass = false;
    //   $('.error_hint').addClass('hidden');
    //   $('#lastname_error_hint').removeClass('hidden');

    //   return;
    // }

    // console.log('lastName', lastName, isEmpty(lastName));

    // // 檢查名字
    // if (isEmpty(firstName)) {
    //   allPass = false;
    //   $('.error_hint').addClass('hidden');
    //   $('#name_error_hint').removeClass('hidden');

    //   return;
    // }

    // // 手機號碼(國碼-手機號碼)
    // if (isEmpty(cellphoneNumber)) {
    //   allPass = false;
    //   $('.error_hint').addClass('hidden');
    //   $('#cellphone_error_hint').removeClass('hidden');

    //   return;
    // }

    // // 聯絡地址
    // if (isEmpty(address)) {
    //   allPass = false;
    //   $('.error_hint').addClass('hidden');
    //   $('#address_error_hint').removeClass('hidden');

    //   return;
    // }

    // // 城市
    // if (isEmpty(city)) {
    //   allPass = false;
    //   $('.error_hint').addClass('hidden');
    //   $('#city_error_hint').removeClass('hidden');

    //   return;
    // }

    // // 州/省/地區
    // if (isEmpty(state)) {
    //   allPass = false;
    //   $('.error_hint').addClass('hidden');
    //   $('#state_error_hint').removeClass('hidden');

    //   return;
    // }

    // // 州/省/地區
    // if (isEmpty(postalCode)) {
    //   allPass = false;
    //   $('.error_hint').addClass('hidden');
    //   $('#postal_code_error_hint').removeClass('hidden');

    //   return;
    // }

    // 檢查 End ==============================================

    // 如果檢查都通過，則開啟確認收件人的燈箱
    if (allPassCount === 7) {
      console.log('allPassCount', allPassCount);
      $('#lightbox_hint').removeClass('hidden');
    }
  });

  // 虛擬獎品
  $('#virtual_prize_form_subnit_btn').bind('click', function () {
    $('#loghtbox_hint').removeClass('hidden');
  });

  // 確認收件人的燈箱選擇 "是"
  $('#makesure_irrecersible_btn').bind('click', function () {
    // 做一些資料處理～

    // 依據不同的獎品類型導到不同的獎品結果頁面
    if (isEntity) {
      location.href = '../entity-prize-result.html';
    } else {
      location.href = '../virtual-prize-result.html';
    }
  });

  // 關閉燈箱的時候reset燈箱內的樣式
  $('#prize_form_close_btn').bind('click', function () {
    $('.error_hint').addClass('hidden');
    $('.form_ctrl').removeClass('readonly');
    $('#entity_prize_form_subnit_btn').removeClass('hidden');
  });
});

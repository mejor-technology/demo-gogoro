$(document).ready(function () {
  $('#left_lottery_btn').bind('click', function () {
    // 減少一次的API處理
    // 轉頁
    location.href = 'select-order.html';
  });

  // 點選抽獎
  $('#before_lottery_img').bind('click', function () {
    // 抽獎動畫處理
    // 等待動畫播完時間(動畫時間加 400 ms)，單位是 ms
    var animationTime = 4000;
    var video = document.getElementById('lottery_animation');

    // 隱藏靜態圖片
    $('#before_lottery_img').addClass('hidden');
    // 隱藏按鈕群
    $('#lottery_btn_group2').addClass('hidden');
    // 顯示影片
    $('#lottery_animation').removeClass('op0');
    // 播放影片
    video.play();

    setTimeout(function () {
      // 顯示確認按鈕
      $('#lottery_btn_group1').removeClass('hidden');
    }, animationTime);

    // 開啟中獎燈箱
    $('.makesure_btn').bind('click', function () {
      $('#loghtbox_prize_form').removeClass('hidden');
    });
  });
});

$(function () {
  // functions =================================================================
  /**
   * @author odin
   * @description modal條件初始化
   */
  function init() {
    if (isAtLogin) {
      console.log('這裡是登入頁');

      if (!shouldCloseEnterModal) {
        sessionStorage.setItem('shouldCloseEnterModal', 'false');
        showModal('#enter-modal');
      } else if (shouldCloseEnterModal === 'false') {
        showModal('#enter-modal');
      } else if (shouldCloseEnterModal === 'true') {
        // 本來就隱藏的，不需要打開
      }
    }
  }

  /**
   * @author odin
   * @param {string} DomId -- 該 Dom 的 id 值
   * @description 打開特定的 modal
   */
  function showModal(DomId) {
    if (DomId === '#enter-modal') {
      openModaltype = 'enter-modal';
    } else if (DomId === '#ranking-modal') {
      openModaltype = 'ranking-modal';
    }

    $modalContainer.removeClass('hidden');
    $modalMask.removeClass('hidden');
    $(DomId).removeClass('hidden');
  }

  /**
   * @author odin
   * @param {string} DomId -- 該 Dom 的 id 值
   * @description 關閉特定的 modal
   */
  function closeModal(DomId) {
    $modalContainer.addClass('hidden');
    $modalMask.addClass('hidden');
    $(DomId).addClass('hidden');
    openModaltype = '';
  }

  // Variables ===========================
  let openModaltype = '';
  const href = window.location.href;
  const isAtLogin = href.indexOf('login') !== -1 ? true : false;
  const shouldCloseEnterModal = sessionStorage.getItem('shouldCloseEnterModal');

  const $modalContainer = $('#modal-container');
  const $modalMask = $('#modal-mask');
  const $modalClose = $('.modal-close');
  const $expertRankingBtn = $('#expert_ranking_btn');

  // Initialize ===========================
  init();

  // Events ===============================
  // 關閉事件
  $modalClose.bind('click', function (e) {
    e.preventDefault();

    const target = $(this).data('target');
    const type = $(this).data('type');

    if (type === 'enter-modal') {
      sessionStorage.setItem('shouldCloseEnterModal', 'true');
    }

    closeModal(target);
  });

  $expertRankingBtn.bind('click', function (e) {
    e.preventDefault();

    showModal('#ranking-modal');
  });

  $modalMask.bind('click', function () {
    if (openModaltype === 'enter-modal') {
      sessionStorage.setItem('shouldCloseEnterModal', 'true');
    }

    closeModal('#' + openModaltype);
  });
});

$(document).ready(function () {
  console.log('login');
  // 綁定事件
  // 送出的時候先檢查
  $('#login_btn').bind('click', function (e) {
    e.preventDefault();

    var allPassCount = 0;

    $('.form_input').each(function () {
      var errorDom = $(this).data('error');
      var value = $(this).val();

      if (isEmpty(value)) {
        $(errorDom).removeClass('hidden');
      } else {
        $(errorDom).addClass('hidden');
        allPassCount++;
      }
    });

    if (allPassCount === 4) {
      console.log('全部都通過檢查');
    }
  });
});

/**
 * @author odin
 * @description 判斷是否該值為空
 * @param  {Primitive value} value 輸入的值
 * @returns {boolean or null}
 */
function isEmpty(value) {
  if (value === 0) {
    return false;
  } else {
    if (value && value !== '' && value !== null && value !== undefined) {
      return false;
    } else {
      return true;
    }
  }
}

/**
 * @author odin
 * @param {function} func - callbackFunc
 * @param {number} delay - 要延遲幾秒執行
 * @description 代理模式 -- 等到最後一次function被觸發的時候，才開始計算timeout，等到timeout結束才抓取資料。
 */
function debounce(func, delay) {
  // timeout 初始值
  let timeout = null;
  return function () {
    let context = this; // 指向 myDebounce 這個 input
    let args = arguments; // KeyboardEvent
    clearTimeout(timeout);

    timeout = setTimeout(function () {
      func.apply(context, args);
    }, delay);
  };
}

/**
 * @author odin
 * @param {string} target - dom selector
 * @description 關閉燈箱
 */
function closeLightbox(target) {
  $(target).addClass('hidden');
}

/**
 * @author odin
 * @description 判斷目前是不是 IE 瀏覽器
 * @return {boolean}}
 */
function isIE() {
  if (
    navigator.userAgent.indexOf('MSIE') !== -1 ||
    navigator.appVersion.indexOf('Trident/') > 0
  ) {
    return true;
  } else {
    return false;
  }
}

/**
 * @author odin
 * @description 如果目前是IE 且 不是 不支援IE支援頁面 的話，則轉跳到 不支援IE支援頁面
 */
function notSupportIECheck() {
  const isNotSupportIEPage =
    location.href.indexOf('no-ie') !== -1 ? true : false;

  if (isIE() && !isNotSupportIEPage) {
    window.location.href = 'no-ie.html';
  }
}

// Initialization =============================================================
notSupportIECheck();

// keyup 事件綁定
$('.form_input').bind(
  'keyup',
  debounce(function () {
    var errorDom = $(this).data('error');
    var value = $(this).val();

    if (isEmpty(value)) {
      $(errorDom).removeClass('hidden');
    } else {
      $(errorDom).addClass('hidden');
    }
  }, 500),
);

$('.close_btn').bind('click', function () {
  var target = $(this).data('target');

  closeLightbox(target);
});

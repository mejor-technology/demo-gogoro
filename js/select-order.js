$(document).ready(function () {
  // 變數宣告
  var selectedOption = '';
  var isDummyOptionsClosed = true;

  // select dummy operation
  $('#select_dummy').bind('click', function () {
    if (isDummyOptionsClosed) {
      $('#dummy_options').removeClass('hidden');
      isDummyOptionsClosed = false;
      // 開啟的時候才關閉說明文字
      // $('.times_text').hide();
      // $('.select_detail').addClass('hidden');
      // 關閉錯誤提示
      $('#select_error_hint').addClass('hidden');
    } else {
      $('#dummy_options').addClass('hidden');
      isDummyOptionsClosed = true;
    }
  });

  // 選擇單一個選項
  $('#select_dummy').on('click', '.dummy_option', function () {
    var value = $(this).data('value') + '';
    var text = $(this).text();
    var hasHidden = $('#select_detail').hasClass('hidden');

    // 修改整體的 padding-bottom

    // 修改下方按鈕的定位
    $('#select-btn-group').removeClass('pos-fixed-bottom');
    $('#main_select').addClass('main-padding-btm-ctrl');

    // 放入資料
    selectedOption = value;
    $('#showing_text').text(text);
    // $('#select_times').val(value);

    // 依據選擇的選項顯示
    if (value === '1' || value === '3' || value === '5' || value === '10') {
      $('.times_text').hide();
      $('#times_' + value).show();
      // 有變動的話就移除隱藏的詳細內容
      hasHidden ? $('#select_detail').removeClass('hidden') : null;
    }
  });

  // 選擇倍率(棄用)
  // $('#select_times').bind('change', function () {
  //   var option = $(this).val();
  //   var hasHidden = $('#select_detail').hasClass('hidden');

  //   // 放入資料
  //   selectedOption = option;

  //   // 依據選擇的選項顯示
  //   if (option === '1' || option === '3' || option === '5' || option === '10') {
  //     $('.times_text').hide();
  //     $('#times_' + option).show();
  //     // 有變動的話就移除隱藏的詳細內容
  //     hasHidden ? $('#select_detail').removeClass('hidden') : null;
  //   } else if (option === '') {
  //     $('#select_detail').addClass('hidden');
  //     $('.times_text').hide();
  //   }
  // });

  // 檢查是否已經選擇倍率 ? 開啟燈箱提示告知連線狀況 : 錯誤提示，請求使用者選擇倍率
  $('#lottery_btn').bind('click', function () {
    console.log('selectedOption', selectedOption);

    // 檢查是否已經選擇倍率
    if (selectedOption) {
      console.log('開始抽獎！！');
      // 關閉錯誤提示
      $('#select_error_hint').hasClass('hidden') === false
        ? $('#select_error_hint').addClass('hidden')
        : null;

      // 開啟燈箱
      $('#loghtbox_select').removeClass('hidden');
    } else {
      // 開啟錯誤提示
      $('#select_error_hint').removeClass('hidden');
    }
  });
});
